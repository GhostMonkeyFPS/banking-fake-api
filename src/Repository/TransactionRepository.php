<?php

namespace EActive\Bundle\BankingFakeAPIBundle\Repository;

use Doctrine\ODM\MongoDB\Repository\DocumentRepository;
use EActive\Bundle\BankingFakeAPIBundle\Document\Account;
use EActive\Bundle\BankingFakeAPIBundle\Document\Transaction;

class TransactionRepository extends DocumentRepository
{
    /**
     * @return Transaction[]
     *
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     */
    public function getTransactionsByAccount(Account $account): array
    {
        $query = $this->createQueryBuilder()
            ->field('account')->equals($account)
            ->sort('createdAt', 'desc')
            ->getQuery();

        return $query->execute()->toArray();
    }
}
