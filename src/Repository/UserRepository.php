<?php

namespace EActive\Bundle\BankingFakeAPIBundle\Repository;

use Doctrine\ODM\MongoDB\Repository\DocumentRepository;
use EActive\Bundle\BankingFakeAPIBundle\Document\User;

class UserRepository extends DocumentRepository
{
    public function findUserByInitialToken(string $token): User
    {
        $query = $this->createQueryBuilder()
            ->field('tokens.initial_token')->equals($token)
            ->getQuery();

        return $query->getSingleResult();
    }

    public function findUserByBearer(string $token): ?User
    {
        $query = $this->createQueryBuilder()
            ->field('tokens.access_token')->equals($token)
            ->getQuery();

        return $query->getSingleResult();
    }

    public function findUserByRefreshToken(string $token): User
    {
        $query = $this->createQueryBuilder()
            ->field('tokens.refresh_token')->equals($token)
            ->getQuery();

        return $query->getSingleResult();
    }
}
