<?php

namespace EActive\Bundle\BankingFakeAPIBundle\Repository;

use Doctrine\ODM\MongoDB\Repository\DocumentRepository;
use EActive\Bundle\BankingFakeAPIBundle\Document\User;

class AccountRepository extends DocumentRepository
{
    public function paginatorAccountsByUser(User $user, int $limit, string $idCursor = null, string $beforeAfter = null)
    {
        if (null == $idCursor && null == $beforeAfter) {
            $query = $this->createQueryBuilder()
                ->field('accounts.')
                ->limit($limit)
                ->getQuery();
        } else {
            $query = $this->createQueryBuilder()
                ->skip(0)
                ->limit($limit)
                ->getQuery();
        }

        return $query->execute();
    }
}
