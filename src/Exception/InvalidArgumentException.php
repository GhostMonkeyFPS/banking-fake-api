<?php

namespace EActive\Bundle\BankingFakeAPIBundle\Exception;

class InvalidArgumentException extends \RuntimeException
{
}
