<?php

namespace EActive\Bundle\BankingFakeAPIBundle\Model;

use Symfony\Component\Serializer\Normalizer\DenormalizableInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Validator\Constraints as Assert;

class SyncRequestModel implements DenormalizableInterface
{
    /**
     * @var string
     * @Assert\Choice(choices={"synchronization"})
     */
    protected $type;

    /**
     * @var string
     * @Assert\Choice(choices={"account"})
     */
    protected $resourceType;

    /**
     * @var string
     */
    protected $resourceId;

    /**
     * @var string
     * @Assert\Choice(choices={"accountDetails", "accountTransactions"})
     */
    protected $subType;

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getResourceType(): string
    {
        return $this->resourceType;
    }

    /**
     * @return string
     */
    public function getResourceId(): string
    {
        return $this->resourceId;
    }

    /**
     * @return string
     */
    public function getSubType(): string
    {
        return $this->subType;
    }

    public function denormalize(DenormalizerInterface $denormalizer, $data, string $format = null, array $context = [])
    {
        if ('json' != $format) {
            return;
        }
        $data = json_decode($data, true);
        $data = $data['data'];

        $this->type = $data['type'];
        $this->resourceType = $data['attributes']['resourceType'];
        $this->resourceId = $data['attributes']['resourceId'];
        $this->subType = $data['attributes']['subtype'];
    }
}
