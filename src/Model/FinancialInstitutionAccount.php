<?php

namespace EActive\Bundle\BankingFakeAPIBundle\Model;

class FinancialInstitutionAccount
{
    protected $type = 'financialInstitutionAccount';

    protected $id;

    protected $subtype;

    protected $referenceType;

    protected $reference;

    protected $product;

    protected $holderName;

    protected $description;

    protected $currentBalanceReferenceDate;

    protected $currentBalanceChangedAt;

    protected $currentBalance;

    protected $currency;

    protected $availableBalanceReferenceDate;

    protected $availableBalanceChangedAt;

    protected $availableBalance;

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): void
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getSubtype()
    {
        return $this->subtype;
    }

    /**
     * @param mixed $subtype
     */
    public function setSubtype($subtype): void
    {
        $this->subtype = $subtype;
    }

    /**
     * @return mixed
     */
    public function getReferenceType()
    {
        return $this->referenceType;
    }

    /**
     * @param mixed $referenceType
     */
    public function setReferenceType($referenceType): void
    {
        $this->referenceType = $referenceType;
    }

    /**
     * @return mixed
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * @param mixed $reference
     */
    public function setReference($reference): void
    {
        $this->reference = $reference;
    }

    /**
     * @return mixed
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param mixed $product
     */
    public function setProduct($product): void
    {
        $this->product = $product;
    }

    /**
     * @return mixed
     */
    public function getHolderName()
    {
        return $this->holderName;
    }

    /**
     * @param mixed $holderName
     */
    public function setHolderName($holderName): void
    {
        $this->holderName = $holderName;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description): void
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getCurrentBalanceReferenceDate()
    {
        return $this->currentBalanceReferenceDate;
    }

    /**
     * @param mixed $currentBalanceReferenceDate
     */
    public function setCurrentBalanceReferenceDate($currentBalanceReferenceDate): void
    {
        $this->currentBalanceReferenceDate = $currentBalanceReferenceDate;
    }

    /**
     * @return mixed
     */
    public function getCurrentBalanceChangedAt()
    {
        return $this->currentBalanceChangedAt;
    }

    /**
     * @param mixed $currentBalanceChangedAt
     */
    public function setCurrentBalanceChangedAt($currentBalanceChangedAt): void
    {
        $this->currentBalanceChangedAt = $currentBalanceChangedAt;
    }

    /**
     * @return mixed
     */
    public function getCurrentBalance()
    {
        return $this->currentBalance;
    }

    /**
     * @param mixed $currentBalance
     */
    public function setCurrentBalance($currentBalance): void
    {
        $this->currentBalance = $currentBalance;
    }

    /**
     * @return mixed
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param mixed $currency
     */
    public function setCurrency($currency): void
    {
        $this->currency = $currency;
    }

    /**
     * @return mixed
     */
    public function getAvailableBalanceReferenceDate()
    {
        return $this->availableBalanceReferenceDate;
    }

    /**
     * @param mixed $availableBalanceReferenceDate
     */
    public function setAvailableBalanceReferenceDate($availableBalanceReferenceDate): void
    {
        $this->availableBalanceReferenceDate = $availableBalanceReferenceDate;
    }

    /**
     * @return mixed
     */
    public function getAvailableBalanceChangedAt()
    {
        return $this->availableBalanceChangedAt;
    }

    /**
     * @param mixed $availableBalanceChangedAt
     */
    public function setAvailableBalanceChangedAt($availableBalanceChangedAt): void
    {
        $this->availableBalanceChangedAt = $availableBalanceChangedAt;
    }

    /**
     * @return mixed
     */
    public function getAvailableBalance()
    {
        return $this->availableBalance;
    }

    /**
     * @param mixed $availableBalance
     */
    public function setAvailableBalance($availableBalance): void
    {
        $this->availableBalance = $availableBalance;
    }
}
