<?php

namespace EActive\Bundle\BankingFakeAPIBundle\Model;

use EActive\Bundle\BankingFakeAPIBundle\Document\Error;

class SyncModel
{
    /**
     * @var string
     */
    protected $status;

    /**
     * @var Error[]
     */
    protected $errors;

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus(string $status): void
    {
        $this->status = $status;
    }

    /**
     * @return Error[]
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    /**
     * @param Error[] $errors
     */
    public function setErrors(array $errors): void
    {
        $this->errors = $errors;
    }
}
