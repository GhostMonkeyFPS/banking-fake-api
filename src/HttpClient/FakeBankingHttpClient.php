<?php

namespace EActive\Bundle\BankingFakeAPIBundle\HttpClient;

use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\HttpClient\MockHttpClient;
use Symfony\Component\HttpClient\Response\MockResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Controller\ArgumentResolverInterface;
use Symfony\Component\HttpKernel\Controller\ControllerResolverInterface;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;
use Symfony\Contracts\HttpClient\ResponseStreamInterface;

/**
 * @method withOptions(array $options)
 */
class FakeBankingHttpClient implements HttpClientInterface
{
    /**
     * @var HttpClientInterface
     */
    private $httpClient;

    /**
     * @var Router
     */
    private $router;

    /**
     * @var ControllerResolverInterface
     */
    private $controllerResolver;

    /**
     * @var ArgumentResolverInterface
     */
    private $argumentResolver;

    /**
     * FakeBankingHttpClient constructor.
     */
    public function __construct(HttpClientInterface $httpClient,
                                Router $router,
                                ControllerResolverInterface $controllerResolver,
                                ArgumentResolverInterface $argumentResolver)
    {
        $this->httpClient = $httpClient;
        $this->router = $router;
        $this->controllerResolver = $controllerResolver;
        $this->argumentResolver = $argumentResolver;
    }

    public function request(string $method, string $url, array $options = []): ResponseInterface
    {
        $content = $options['body'] ?? [];
        $parameters = $options['body'] ?? $options['query'] ?? [];
        $parameters = (is_string($parameters)) ? json_decode($parameters, true) : $parameters;

        $request = Request::create($url, $method, $parameters, [], [], [], json_encode($content));
        $request->headers->add($options['headers']);

        $requestContext = (new RequestContext())->fromRequest($request);
        $matcher = new UrlMatcher($this->router->getRouteCollection(), $requestContext);

        try {
            $request->attributes->add($matcher->match($request->getPathInfo()));

            $controller = $this->controllerResolver->getController($request);
            $arguments = $this->argumentResolver->getArguments($request, $controller);

            /** @var Response $response */
            $response = call_user_func_array($controller, $arguments);

            $info = [
                'canceled' => false,
                'http_code' => $response->getStatusCode(),
                'http_method' => $request->getMethod(),
                'redirect_count' => 0,
                'response_headers' => $response->headers->all(),
                'start_time' => $response->getAge(),
                'url' => $request->getBasePath(),
            ];

            $client = new MockHttpClient([new MockResponse($response->getContent(), $info)]);

            return $client->request($request->getMethod(), $url, $options);
        } catch (\Exception $exception) {
            dump($exception);
        } finally {

            $info = [
                'canceled' => false,
                'http_code' => isset($response) ? $response->getStatusCode() : 500,
                'http_method' => $request->getMethod(),
                'redirect_count' => 0,
                'response_headers' => isset($response) ? $response->headers->all() : [],
                'start_time' => isset($response) ? $response->getAge() : 1.0,
                'url' => $request->getBasePath(),
            ];

            $body = isset($response) ? $response->getContent() : '';
            $client = new MockHttpClient([new MockResponse($body, $info)]);

            return $client->request($request->getMethod(), $url, $options);
        }
    }

    public function stream($responses, float $timeout = null): ResponseStreamInterface
    {
    }

    public function __call($name, $arguments)
    {
    }
}
