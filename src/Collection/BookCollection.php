<?php

namespace EActive\Bundle\BankingFakeAPIBundle\Collection;

use App\Entity\BaseEntity;
use ArrayIterator;
use Closure;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use EActive\Bundle\BankingFakeAPIBundle\Document\BaseDocument;
use EActive\Bundle\BankingFakeAPIBundle\Exception\InvalidArgumentException;
use Symfony\Component\Intl\Exception\MethodNotImplementedException;

/**
 * @phpstan-template TKey
 * @psalm-template TKey of array-key
 * @psalm-template T
 * @template-extends Collection<TKey, T>
 */
class BookCollection implements Collection
{
    /**
     * @var Collection
     */
    private $collection;

    /**
     * @var string
     */
    private $cursor;

    /**
     * @var string
     */
    private $beforeOrAfter;

    /**
     * @var int
     */
    private $limit;

    /**
     * @var int
     */
    private $cursorSpottedIndex;

    /**
     * @var BaseEntity|bool|null
     */
    private $before;

    /**
     * @var BaseEntity|bool|null
     */
    private $after;

    /**
     * BookCollection constructor.
     */
    public function __construct(Collection $collection, string $cursor = null, string $beforeOrAfter = 'before', int $limit = 100)
    {
        $this->collection = $collection;
        $this->cursor = $cursor;
        $this->beforeOrAfter = $beforeOrAfter;
        $this->limit = $limit;

        $this->spotCursor();
        $this->setBeforeAfter();
    }

    public function add($element)
    {
        throw new MethodNotImplementedException('add');
    }

    public function clear()
    {
        throw new MethodNotImplementedException('clear');
    }

    public function contains($element): bool
    {
        if ('after' === $this->beforeOrAfter) {
            for ($i = $this->cursorSpottedIndex; $i < $this->collection->count(); ++$i) {
                if ($this->collection[$i] == $element) {
                    return true;
                }
            }

            return false;
        }
        if ('before' === $this->beforeOrAfter) {
            for ($i = 0; $i < $this->cursorSpottedIndex; ++$i) {
                if ($this->collection[$i] == $element) {
                    return true;
                }
            }

            return false;
        }

        throw new InvalidArgumentException('Invalid beforeOrAfter');
    }

    public function isEmpty()
    {
        throw new MethodNotImplementedException('isEmpty');
    }

    public function remove($key)
    {
        throw new MethodNotImplementedException('remove');
    }

    public function removeElement($element)
    {
        throw new MethodNotImplementedException('removeElement');
    }

    public function containsKey($key)
    {
        throw new MethodNotImplementedException('containsKey');
    }

    public function get($key)
    {
        // TODO: Implement get() method.
    }

    public function getKeys()
    {
        // TODO: Implement getKeys() method.
    }

    public function getValues()
    {
        // TODO: Implement getValues() method.
    }

    public function set($key, $value)
    {
        // TODO: Implement set() method.
    }

    public function toArray()
    {
        if ('after' === $this->beforeOrAfter) {
            return $this->collection->slice($this->cursorSpottedIndex);
        } elseif ('before' === $this->beforeOrAfter) {
            return $this->collection->slice(0, $this->cursorSpottedIndex);
        }

        throw new InvalidArgumentException('Invalid beforeOrAfter');
    }

    public function first()
    {
        if ('after' === $this->beforeOrAfter) {
            return $this->collection->slice($this->cursorSpottedIndex);
        } elseif ('before' === $this->beforeOrAfter) {
            return $this->collection->slice(0, $this->cursorSpottedIndex);
        }

        throw new InvalidArgumentException('Invalid beforeOrAfter');
    }

    public function last()
    {
        // TODO: Implement last() method.
    }

    public function key()
    {
        // TODO: Implement key() method.
    }

    public function current()
    {
        // TODO: Implement current() method.
    }

    public function next()
    {
        // TODO: Implement next() method.
    }

    public function exists(Closure $p)
    {
        // TODO: Implement exists() method.
    }

    public function filter(Closure $p)
    {
        // TODO: Implement filter() method.
    }

    public function forAll(Closure $p)
    {
        // TODO: Implement forAll() method.
    }

    public function map(Closure $func)
    {
        // TODO: Implement map() method.
    }

    public function partition(Closure $p)
    {
        // TODO: Implement partition() method.
    }

    public function indexOf($element)
    {
        // TODO: Implement indexOf() method.
    }

    public function slice($offset, $length = null)
    {
        // TODO: Implement slice() method.
    }

    public function getIterator()
    {
        if (null == $this->cursor) {
            return new ArrayIterator($this->collection->slice(0, $this->limit));
        } elseif ('after' === $this->beforeOrAfter) {
            return new ArrayIterator($this->collection->slice($this->cursorSpottedIndex + 1, $this->limit));
        } elseif ('before' === $this->beforeOrAfter) {
            $offset = ($this->cursorSpottedIndex - $this->limit >= 0) ? $this->cursorSpottedIndex - $this->limit : 0;
            $limit = ($this->cursorSpottedIndex - $this->limit >= 0) ? $this->limit : $this->cursorSpottedIndex;
            return new ArrayIterator($this->collection->slice($offset, $limit));
        }

        throw new InvalidArgumentException('Invalid beforeOrAfter');
    }

    public function offsetExists($offset)
    {
        // TODO: Implement offsetExists() method.
    }

    public function offsetGet($offset)
    {
        // TODO: Implement offsetGet() method.
    }

    public function offsetSet($offset, $value)
    {
        // TODO: Implement offsetSet() method.
    }

    public function offsetUnset($offset)
    {
        // TODO: Implement offsetUnset() method.
    }

    public function count()
    {
        // TODO: Implement count() method.
    }

    public function getBeforeId(): ?string
    {
        return (isset($this->before) && $this->before) ? $this->before->getId() : null;
    }

    public function getAfterId(): ?string
    {
        return (isset($this->after) && $this->after) ? $this->after->getId() : null;
    }

    private function spotCursor(): void
    {
        if (null == $this->cursor) {
            $this->cursorSpottedIndex = 0;

            return;
        }

        $this->cursorSpottedIndex = 0;
        /** @var BaseDocument $item */
        foreach ($this->collection as $item) {
            if ($item->getId() == $this->cursor) {
                break;
            }
            ++$this->cursorSpottedIndex;
        }
    }

    private function setBeforeAfter(): void
    {
        if (null == $this->cursor) {
            $array = new ArrayCollection($this->collection->slice(0, $this->limit));

            $this->after = ($this->collection->last() != $array->last()) ? $array->last() : null;
            $this->before = null;

            return;
        } elseif ('after' === $this->beforeOrAfter) {
            $array = new ArrayCollection($this->collection->slice($this->cursorSpottedIndex + 1, $this->limit));

            $this->before = ($this->collection->first() != $array->first()) ? $array->first() : null;
            $this->after = ($this->collection->last() != $array->last()) ? $array->last() : null;

            return;
        } elseif ('before' === $this->beforeOrAfter) {
            $offset = ($this->cursorSpottedIndex - $this->limit >= 0) ? $this->cursorSpottedIndex - $this->limit : 0;
            $limit = ($this->cursorSpottedIndex - $this->limit >= 0) ? $this->limit : $this->cursorSpottedIndex;
            $array = new ArrayCollection($this->collection->slice($offset, $limit));

            $this->before = ($this->collection->first() != $array->first()) ? $array->first() : null;
            $this->after = ($this->collection->last() != $array->last()) ? $array->last() : null;

            return;
        }

        throw new InvalidArgumentException('Invalid beforeOrAfter');
    }
}
