<?php

namespace EActive\Bundle\BankingFakeAPIBundle\Document;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @MongoDB\Document(collection="fake_sync")
 * @MongoDB\HasLifecycleCallbacks
 */
class Synchronization extends BaseDocument
{
    /**
     * @var string
     * @MongoDB\Field(type="string")
     * @Assert\Choice(choices={"accountTransactions", "accountDetail"}, message="Invalid subtype.")
     */
    protected $subType;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     * @Assert\Choice(choices={"pending", "running", "success", "error"}, message="Invalid status.")
     */
    protected $status;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     * @Assert\Choice(choices={"account"}, message="Invalid resourceType.")
     */
    protected $resourceType;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     */
    protected $resourceId;

    /**
     * @var Collection<Error>
     * @MongoDB\EmbedMany(targetDocument="EActive\Bundle\BankingFakeAPIBundle\Document\Error")
     */
    protected $errors;

    /**
     * @var Account
     * @MongoDB\ReferenceOne(targetDocument="EActive\Bundle\BankingFakeAPIBundle\Document\Account")
     */
    protected $account;

    public function __construct()
    {
        parent::__construct();
        $this->resourceType = 'account';
        $this->status = 'success';
        $this->errors = new ArrayCollection();
    }

    public function getSubType(): string
    {
        return $this->subType;
    }

    public function setSubType(string $subType): void
    {
        $this->subType = $subType;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function setStatus(string $status): void
    {
        $this->status = $status;
    }

    public function getResourceType(): string
    {
        return $this->resourceType;
    }

    public function setResourceType(string $resourceType): void
    {
        $this->resourceType = $resourceType;
    }

    public function getResourceId(): string
    {
        return $this->resourceId;
    }

    public function setResourceId(string $resourceId): void
    {
        $this->resourceId = $resourceId;
    }

    /**
     * @return Collection<Error>
     */
    public function getErrors(): Collection
    {
        return $this->errors;
    }

    /**
     * @param Collection $errors
     */
    public function setErrors(Collection $errors): void
    {
        $this->errors = $errors;
    }

    public function addErrors(Error $error): void
    {
        $this->errors[] = $error;
    }

    /**
     * @return Account
     */
    public function getAccount(): Account
    {
        return $this->account;
    }

    /**
     * @param Account $account
     */
    public function setAccount(Account $account): void
    {
        $this->account = $account;
    }
}
