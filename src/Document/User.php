<?php

namespace EActive\Bundle\BankingFakeAPIBundle\Document;

use Doctrine\Common\Collections\Collection;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Doctrine\ODM\MongoDB\Mapping\Annotations\ReferenceMany;
use EActive\Bundle\BankingFakeAPIBundle\Document\Auth\Token;
use Symfony\Component\Uid\UuidV4;

/**
 * @MongoDB\Document(collection="fake_user",
 *     repositoryClass="EActive\Bundle\BankingFakeAPIBundle\Repository\UserRepository")
 */
class User
{
    /**
     * @var string
     * @MongoDB\Id(type="string", strategy="none")
     */
    protected $id;

    /**
     * @var Collection<Account>
     * @ReferenceMany("EActive\Bundle\BankingFakeAPIBundle\Document\Account")
     */
    protected $accounts;

    /**
     * @var Token
     * @MongoDB\EmbedOne(targetDocument="EActive\Bundle\BankingFakeAPIBundle\Document\Auth\Token")
     */
    protected $tokens;

    /**
     * User constructor.
     */
    public function __construct()
    {
        $this->id = (new UuidV4())->__toString();
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(string $id): void
    {
        $this->id = $id;
    }

    /**
     * @return Collection<Account>
     */
    public function getAccounts(): Collection
    {
        return $this->accounts;
    }

    /**
     * @param Collection<Account> $accounts
     */
    public function setAccounts(Collection $accounts): void
    {
        $this->accounts = $accounts;
    }

    public function addAccount(Account $account): void
    {
        $this->accounts[] = $account;
    }

    public function getTokens(): Token
    {
        return $this->tokens;
    }

    public function setTokens(Token $tokens): void
    {
        $this->tokens = $tokens;
    }
}
