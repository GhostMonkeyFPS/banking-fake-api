<?php

namespace EActive\Bundle\BankingFakeAPIBundle\Document;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Symfony\Component\Uid\UuidV4;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @MongoDB\Document(collection="fake_account",
 *     repositoryClass="EActive\Bundle\BankingFakeAPIBundle\Repository\AccountRepository")
 */
class Account
{
    /**
     * @var string
     * @MongoDB\Id(type="string", strategy="none")
     */
    protected $id;

    /**
     * @var \DateTime
     * @MongoDB\Field(type="date")
     */
    protected $synchronizedAt;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     *
     * @Assert\NotBlank
     * @Assert\Choice(choices={"checking", "savings", "securities"}, message="Invalid subtype.")
     */
    protected $subType;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     *
     * @Assert\NotBlank
     * @Assert\Choice(choices={"IBAN"}, message="Invalid referenceType.")
     */
    protected $referenceType;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     *
     * @Assert\NotBlank
     * @Assert\Iban(message="Invalid reference.")
     */
    protected $reference;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     * @Assert\NotBlank
     */
    protected $product;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     */
    protected $internalReference;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     *
     * @Assert\NotBlank
     */
    protected $holderName;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     *
     * @Assert\NotBlank
     */
    protected $description;

    /**
     * @var bool
     * @MongoDB\Field(type="bool")
     */
    protected $deprecated;

    /**
     * @var \DateTime
     * @MongoDB\Field(type="date")
     */
    protected $currentBalanceVariationObservedAt;

    /**
     * @var \DateTime
     * @MongoDB\Field(type="date")
     *
     * @Assert\NotBlank
     */
    protected $currentBalanceReferenceDate;

    /**
     * @var \DateTime
     * @MongoDB\Field(type="date")
     *
     * @Assert\NotBlank
     */
    protected $currentBalanceChangedAt;

    /**
     * @var float
     * @MongoDB\Field(type="float")
     *
     * @Assert\NotBlank
     */
    protected $currentBalance;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     *
     * @Assert\Currency
     */
    protected $currency;

    /**
     * @var \DateTime
     * @MongoDB\Field(type="date")
     */
    protected $availableBalanceVariationObservedAt;

    /**
     * @var \DateTime
     * @MongoDB\Field(type="date")
     *
     * @Assert\NotBlank
     */
    protected $availableBalanceReferenceDate;

    /**
     * @var \DateTime
     * @MongoDB\Field(type="date")
     *
     * @Assert\NotBlank
     */
    protected $availableBalanceChangedAt;

    /**
     * @var float
     * @MongoDB\Field(type="float")
     *
     * @Assert\NotBlank
     */
    protected $availableBalance;

    /**
     * @var \DateTime|null
     * @MongoDB\Field(type="date")
     */
    protected $authorizedAt;

    /**
     * @var \DateTime|null
     * @MongoDB\Field(type="date")
     */
    protected $authorizationExpirationExpectedAt;

    /**
     * @var Collection<Transaction>
     * @MongoDB\ReferenceMany(targetDocument="EActive\Bundle\BankingFakeAPIBundle\Document\Transaction")
     */
    protected $transactions;

    /**
     * @var Collection<Synchronization>
     * @MongoDB\ReferenceMany(targetDocument="EActive\Bundle\BankingFakeAPIBundle\Document\Synchronization",
     *     cascade={"persist"})
     */
    protected $synchronizations;

    /**
     * Account constructor.
     */
    public function __construct()
    {
        $this->id = (new UuidV4())->__toString();
        $this->synchronizedAt = new \DateTime();
        $this->internalReference = (new UuidV4())->__toString();
        $this->deprecated = false;
        $this->currentBalanceVariationObservedAt = new \DateTime();
        $this->currentBalanceChangedAt = new \DateTime();
        $this->availableBalanceVariationObservedAt = new \DateTime();
        $this->authorizedAt = null;
        $this->authorizationExpirationExpectedAt = null;
        $this->transactions = new ArrayCollection();
        $this->synchronizations = new ArrayCollection();
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(string $id): void
    {
        $this->id = $id;
    }

    public function getSynchronizedAt(): \DateTime
    {
        return $this->synchronizedAt;
    }

    public function setSynchronizedAt(\DateTime $synchronizedAt): void
    {
        $this->synchronizedAt = $synchronizedAt;
    }

    public function getSubType(): string
    {
        return $this->subType;
    }

    public function setSubType(string $subType): void
    {
        $this->subType = $subType;
    }

    public function getReferenceType(): string
    {
        return $this->referenceType;
    }

    public function setReferenceType(string $referenceType): void
    {
        $this->referenceType = $referenceType;
    }

    public function getReference(): string
    {
        return $this->reference;
    }

    public function setReference(string $reference): void
    {
        $this->reference = $reference;
    }

    public function getProduct(): string
    {
        return $this->product;
    }

    public function setProduct(string $product): void
    {
        $this->product = $product;
    }

    public function getInternalReference(): string
    {
        return $this->internalReference;
    }

    public function setInternalReference(string $internalReference): void
    {
        $this->internalReference = $internalReference;
    }

    public function getHolderName(): string
    {
        return $this->holderName;
    }

    public function setHolderName(string $holderName): void
    {
        $this->holderName = $holderName;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    public function isDeprecated(): bool
    {
        return $this->deprecated;
    }

    public function setDeprecated(bool $deprecated): void
    {
        $this->deprecated = $deprecated;
    }

    public function getCurrentBalanceVariationObservedAt(): \DateTime
    {
        return $this->currentBalanceVariationObservedAt;
    }

    public function setCurrentBalanceVariationObservedAt(\DateTime $currentBalanceVariationObservedAt): void
    {
        $this->currentBalanceVariationObservedAt = $currentBalanceVariationObservedAt;
    }

    public function getCurrentBalanceReferenceDate(): \DateTime
    {
        return $this->currentBalanceReferenceDate;
    }

    public function setCurrentBalanceReferenceDate(\DateTime $currentBalanceReferenceDate): void
    {
        $this->currentBalanceReferenceDate = $currentBalanceReferenceDate;
    }

    public function getCurrentBalanceChangedAt(): \DateTime
    {
        return $this->currentBalanceChangedAt;
    }

    public function setCurrentBalanceChangedAt(\DateTime $currentBalanceChangedAt): void
    {
        $this->currentBalanceChangedAt = $currentBalanceChangedAt;
    }

    public function getCurrentBalance(): float
    {
        return $this->currentBalance;
    }

    public function setCurrentBalance(float $currentBalance): void
    {
        $this->currentBalance = $currentBalance;
    }

    public function getCurrency(): string
    {
        return $this->currency;
    }

    public function setCurrency(string $currency): void
    {
        $this->currency = $currency;
    }

    public function getAvailableBalanceVariationObservedAt(): \DateTime
    {
        return $this->availableBalanceVariationObservedAt;
    }

    public function setAvailableBalanceVariationObservedAt(\DateTime $availableBalanceVariationObservedAt): void
    {
        $this->availableBalanceVariationObservedAt = $availableBalanceVariationObservedAt;
    }

    public function getAvailableBalanceReferenceDate(): \DateTime
    {
        return $this->availableBalanceReferenceDate;
    }

    public function setAvailableBalanceReferenceDate(\DateTime $availableBalanceReferenceDate): void
    {
        $this->availableBalanceReferenceDate = $availableBalanceReferenceDate;
    }

    public function getAvailableBalanceChangedAt(): \DateTime
    {
        return $this->availableBalanceChangedAt;
    }

    public function setAvailableBalanceChangedAt(\DateTime $availableBalanceChangedAt): void
    {
        $this->availableBalanceChangedAt = $availableBalanceChangedAt;
    }

    public function getAvailableBalance(): float
    {
        return $this->availableBalance;
    }

    public function setAvailableBalance(float $availableBalance): void
    {
        $this->availableBalance = $availableBalance;
    }

    public function getAuthorizedAt(): ?\DateTime
    {
        return $this->authorizedAt;
    }

    public function setAuthorizedAt(\DateTime $authorizedAt): void
    {
        $this->authorizedAt = $authorizedAt;
    }

    public function getAuthorizationExpirationExpectedAt(): ?\DateTime
    {
        return $this->authorizationExpirationExpectedAt;
    }

    public function setAuthorizationExpirationExpectedAt(\DateTime $authorizationExpirationExpectedAt): void
    {
        $this->authorizationExpirationExpectedAt = $authorizationExpirationExpectedAt;
    }

    /**
     * @return Collection
     */
    public function getTransactions()
    {
        return $this->transactions;
    }

    /**
     * @param Transaction $transaction
     */
    public function addTransactions(Transaction $transaction): void
    {
        $this->transactions[] = $transaction;
        $transaction->setAccount($this);
    }

    /**
     * @return Collection<Synchronization
     */
    public function getSynchronizations(): Collection
    {
        return $this->synchronizations;
    }

    /**
     * @param Synchronization $synchronization
     */
    public function addSynchronization(Synchronization $synchronization): void
    {
        $this->synchronizations[] = $synchronization;
        $synchronization->setAccount($this);
    }
}
