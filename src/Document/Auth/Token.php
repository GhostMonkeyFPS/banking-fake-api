<?php

namespace EActive\Bundle\BankingFakeAPIBundle\Document\Auth;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\EmbeddedDocument
 */
class Token
{
    /**
     * @var string
     * @MongoDB\Field(type="string")
     */
    protected $initial_token;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     */
    protected $access_token;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     */
    protected $refresh_token;

    /**
     * @var \DateTime
     * @MongoDB\Field(type="date")
     */
    protected $accessTokenCreatedAt;

    /**
     * @var \DateTime
     * @MongoDB\Field(type="date")
     */
    protected $refreshTokenCreatedAt;

    public function getInitialToken(): string
    {
        return $this->initial_token;
    }

    public function setInitialToken(string $initial_token): void
    {
        $this->initial_token = $initial_token;
    }

    public function getAccessToken(): string
    {
        return $this->access_token;
    }

    public function setAccessToken(string $access_token): void
    {
        $this->access_token = $access_token;
        $this->accessTokenCreatedAt = new \DateTime();
    }

    public function getRefreshToken(): string
    {
        return $this->refresh_token;
    }

    public function setRefreshToken(string $refresh_token): void
    {
        $this->refresh_token = $refresh_token;
        $this->refreshTokenCreatedAt = new \DateTime();
    }

    public function getAccessTokenCreatedAt(): \DateTime
    {
        return $this->accessTokenCreatedAt;
    }

    public function setAccessTokenCreatedAt(\DateTime $accessTokenCreatedAt): void
    {
        $this->accessTokenCreatedAt = $accessTokenCreatedAt;
    }

    public function getRefreshTokenCreatedAt(): \DateTime
    {
        return $this->refreshTokenCreatedAt;
    }

    public function setRefreshTokenCreatedAt(\DateTime $refreshTokenCreatedAt): void
    {
        $this->refreshTokenCreatedAt = $refreshTokenCreatedAt;
    }
}
