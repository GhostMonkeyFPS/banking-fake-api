<?php

namespace EActive\Bundle\BankingFakeAPIBundle\Document;

use Symfony\Component\Uid\UuidV4;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\HasLifecycleCallbacks
 */
abstract class BaseDocument
{
    /**
     * @var string
     * @MongoDB\Id(type="string", strategy="none")
     */
    protected $id;

    /**
     * @var \DateTime
     * @MongoDB\Field(type="date")
     */
    protected $createdAt;

    /**
     * @var \DateTime
     * @MongoDB\Field(type="date")
     */
    protected $updatedAt;

    /**
     * BaseDocument constructor.
     */
    public function __construct()
    {
        $this->id = (new UuidV4())->__toString();
        $this->updateTimestamps();
    }

    /**
     * @MongoDB\PrePersist
     * @MongoDB\PreUpdate
     */
    public function updateTimestamps(): void
    {
        $this->setUpdatedAt(new \DateTime('now'));
        if (null === $this->createdAt) {
            $this->setCreatedAt(new \DateTime('now'));
        }
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(string $id): void
    {
        $this->id = $id;
    }

    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTime $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    public function getUpdatedAt(): \DateTime
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTime $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }
}
