<?php

namespace EActive\Bundle\BankingFakeAPIBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Symfony\Component\Uid\UuidV4;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @MongoDB\Document(collection="fake_transaction",
 *     repositoryClass="EActive\Bundle\BankingFakeAPIBundle\Repository\TransactionRepository")
 */
class Transaction extends BaseDocument
{
    /**
     * @var \DateTime
     * @MongoDB\Field(type="date")
     *
     * @Assert\NotBlank
     */
    protected $valueDate;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     *
     * @Assert\Choice(choices={"structured", "unstructed"}, message="Invalid remittanceInformationType.")
     */
    protected $remittanceInformationType;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     */
    protected $remittanceInformation;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     */
    protected $purposeCode;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     */
    protected $proprietaryBankTransactionCode;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     */
    protected $mandateId;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     */
    protected $internalReference;

    /**
     * @var \DateTime
     * @MongoDB\Field(type="date")
     *
     * @Assert\NotBlank
     */
    protected $executionDate;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     */
    protected $endToEndId;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     */
    protected $digest;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     *
     * @Assert\NotBlank
     */
    protected $description;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     *
     * @Assert\Choice(choices={"EUR"}, message="Invalid currency.")
     */
    protected $currency;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     */
    protected $creditorId;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     */
    protected $counterpartReference;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     */
    protected $counterpartName;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     */
    protected $bankTransactionCode;

    /**
     * @var float
     * @MongoDB\Field(type="float")
     *
     * @Assert\NotBlank
     */
    protected $amount;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     */
    protected $additionalInformation;

    /**
     * @var Account
     * @MongoDB\ReferenceOne(targetDocument="EActive\Bundle\BankingFakeAPIBundle\Document\Account")
     */
    protected $account;

    /**
     * Transaction constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->internalReference = (new UuidV4())->__toString();
        $this->digest = '';
    }

    public function getValueDate(): \DateTime
    {
        return $this->valueDate;
    }

    public function setValueDate(\DateTime $valueDate): void
    {
        $this->valueDate = $valueDate;
    }

    public function getRemittanceInformationType(): string
    {
        return $this->remittanceInformationType;
    }

    public function setRemittanceInformationType(string $remittanceInformationType): void
    {
        $this->remittanceInformationType = $remittanceInformationType;
    }

    public function getRemittanceInformation(): string
    {
        return $this->remittanceInformation;
    }

    public function setRemittanceInformation(string $remittanceInformation): void
    {
        $this->remittanceInformation = $remittanceInformation;
    }

    public function getPurposeCode(): string
    {
        return $this->purposeCode;
    }

    public function setPurposeCode(string $purposeCode): void
    {
        $this->purposeCode = $purposeCode;
    }

    public function getProprietaryBankTransactionCode(): string
    {
        return $this->proprietaryBankTransactionCode;
    }

    public function setProprietaryBankTransactionCode(string $proprietaryBankTransactionCode): void
    {
        $this->proprietaryBankTransactionCode = $proprietaryBankTransactionCode;
    }

    public function getMandateId(): string
    {
        return $this->mandateId;
    }

    public function setMandateId(string $mandateId): void
    {
        $this->mandateId = $mandateId;
    }

    public function getInternalReference(): string
    {
        return $this->internalReference;
    }

    public function setInternalReference(string $internalReference): void
    {
        $this->internalReference = $internalReference;
    }

    public function getExecutionDate(): \DateTime
    {
        return $this->executionDate;
    }

    public function setExecutionDate(\DateTime $executionDate): void
    {
        $this->executionDate = $executionDate;
    }

    public function getEndToEndId(): string
    {
        return $this->endToEndId;
    }

    public function setEndToEndId(string $endToEndId): void
    {
        $this->endToEndId = $endToEndId;
    }

    public function getDigest(): string
    {
        return $this->digest;
    }

    public function setDigest(string $digest): void
    {
        $this->digest = $digest;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    public function getCurrency(): string
    {
        return $this->currency;
    }

    public function setCurrency(string $currency): void
    {
        $this->currency = $currency;
    }

    public function getCreditorId(): string
    {
        return $this->creditorId;
    }

    public function setCreditorId(string $creditorId): void
    {
        $this->creditorId = $creditorId;
    }

    public function getCounterpartReference(): string
    {
        return $this->counterpartReference;
    }

    public function setCounterpartReference(string $counterpartReference): void
    {
        $this->counterpartReference = $counterpartReference;
    }

    public function getCounterpartName(): string
    {
        return $this->counterpartName;
    }

    public function setCounterpartName(string $counterpartName): void
    {
        $this->counterpartName = $counterpartName;
    }

    public function getBankTransactionCode(): string
    {
        return $this->bankTransactionCode;
    }

    public function setBankTransactionCode(string $bankTransactionCode): void
    {
        $this->bankTransactionCode = $bankTransactionCode;
    }

    public function getAmount(): float
    {
        return $this->amount;
    }

    public function setAmount(float $amount): void
    {
        $this->amount = $amount;
    }

    public function getAdditionalInformation(): string
    {
        return $this->additionalInformation;
    }

    public function setAdditionalInformation(string $additionalInformation): void
    {
        $this->additionalInformation = $additionalInformation;
    }

    public function getAccount(): Account
    {
        return $this->account;
    }

    public function setAccount(Account $account): void
    {
        $this->account = $account;
    }
}
