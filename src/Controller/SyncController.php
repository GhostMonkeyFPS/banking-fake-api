<?php

namespace EActive\Bundle\BankingFakeAPIBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ODM\MongoDB\DocumentManager;
use EActive\Bundle\BankingFakeAPIBundle\Document\Synchronization;
use EActive\Bundle\BankingFakeAPIBundle\Model\SyncModel;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class SyncController extends AbstractController
{
    /**
     * @var DocumentManager
     */
    private $documentManager;

    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    public function __construct(DocumentManager $documentManager,
                                SerializerInterface $serializer,
                                ValidatorInterface $validator
    ) {
        $this->documentManager = $documentManager;
        $this->serializer = $serializer;
        $this->validator = $validator;
    }

    public function index(string $id): Response
    {
        $repo = $this->documentManager->getRepository(Synchronization::class);

        return $this->render('@BankingFakeAPI/synchronization/index.html.twig', [
            'synchronization' => $repo->find($id),
        ]);
    }

    public function update(Request $request, string $id): Response
    {
        /** @var SyncModel $serializedSync */
        $serializedSync = $this->serializer->deserialize($request->getContent(), SyncModel::class, 'json');

        $sync = $this->documentManager->getRepository(Synchronization::class)
            ->find($id);

        $sync->setStatus($serializedSync->getStatus());
        $sync->setErrors(new ArrayCollection($serializedSync->getErrors()));


        $this->documentManager->persist($sync);
        $this->documentManager->flush();

        return new JsonResponse(['status' => 'ok']);
    }
}
