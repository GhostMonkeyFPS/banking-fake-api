<?php

namespace EActive\Bundle\BankingFakeAPIBundle\Controller;

use Doctrine\ODM\MongoDB\DocumentManager;
use EActive\Bundle\BankingFakeAPIBundle\Document\Account;
use EActive\Bundle\BankingFakeAPIBundle\Document\Synchronization;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class DashboardController extends AbstractController
{
    /**
     * @var DocumentManager
     */
    private $documentManager;

    public function __construct(DocumentManager $documentManager)
    {
        $this->documentManager = $documentManager;
    }

    public function index(): Response
    {
        $repoAccount = $this->documentManager->getRepository(Account::class);
        $repoSynchronization = $this->documentManager->getRepository(Synchronization::class);

        return $this->render('@BankingFakeAPI/dashboard/index_dashboard.html.twig', [
            'synchronizations' => $repoSynchronization->findBy(['status' => 'pending']),
            'accounts' => $repoAccount->findAll(),
        ]);
    }
}
