<?php

namespace EActive\Bundle\BankingFakeAPIBundle\Controller;

use Doctrine\ODM\MongoDB\DocumentManager;
use EActive\Bundle\BankingFakeAPIBundle\Document\Account;
use EActive\Bundle\BankingFakeAPIBundle\Document\Transaction;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Serializer\Exception\NotNormalizableValueException;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class TransactionController extends AbstractController
{
    /**
     * @var DocumentManager
     */
    private $documentManager;

    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    public function __construct(DocumentManager $documentManager, SerializerInterface $serializer, ValidatorInterface $validator)
    {
        $this->documentManager = $documentManager;
        $this->serializer = $serializer;
        $this->validator = $validator;
    }

    public function index(string $idAccount): Response
    {
        $account = $this->documentManager->getRepository(Account::class)
            ->find($idAccount);

        if (null == $account) {
            throw $this->createNotFoundException('No account found for id '.$idAccount);
        }

        $transactions = $this->documentManager->getRepository(Transaction::class)
            ->findBy(['account' => $account]);

        return $this->render('@BankingFakeAPI/transaction/index.html.twig', [
            'transactions' => $transactions,
        ]);
    }

    public function createTransaction(): Response
    {
        return $this->render('@BankingFakeAPI/transaction/create.html.twig');
    }

    public function createTransactionPost(Request $request, string $idAccount): JsonResponse
    {
        $account = $this->documentManager->getRepository(Account::class)
            ->find($idAccount);

        if (null == $account) {
            throw $this->createNotFoundException('No account found for id '.$idAccount);
        }

        try {
            /** @var Transaction $transaction */
            $transaction = $this->serializer->deserialize($request->getContent(), Transaction::class, 'json');
        } catch (NotNormalizableValueException $exception) {
            return new JsonResponse(['error' => $exception->getMessage()], 422);
        }

        $errors = $this->validator->validate($transaction);

        if (\count($errors) > 0) {
            return new JsonResponse(['error' => $errors[0]->getMessage()], 422);
        }

        $account->addTransactions($transaction);

        $this->documentManager->persist($transaction);
        $this->documentManager->flush();

        return new JsonResponse(['status' => 'ok'], 200);
    }

    public function downloadExample(): Response
    {
        $url = $this->container->get('kernel')
                ->getProjectDir().'/public/bundles/bankingfakeapi/account.json';

        $response = new BinaryFileResponse($url);
        $response->headers->set('Content-Type', 'application/json');
        $response->headers->set('Content-Disposition', 'attachment; filename="account.json"');

        return $response;
    }

    public function upload(Request $request): Response
    {
        if (!$request->files->get('file') instanceof UploadedFile) {
            throw new UnauthorizedHttpException('invalid file');
        }

        if ($content = json_decode($request->files->get('file')->getContent(), true)) {
            $transaction = new Transaction();

            $transaction->setValueDate(new \DateTime($content['valueDate']));
            $transaction->setRemittanceInformationType($content['remittanceInformationType']);
            $transaction->setRemittanceInformation($content['remittanceInformation']);
            $transaction->setPurposeCode($content['purposeCode']);
            $transaction->setProprietaryBankTransactionCode($content['proprietaryBankTransactionCode']);
            $transaction->setMandateId($content['mandateId']);
            $transaction->setInternalReference($content['internalReference']);
            $transaction->setExecutionDate(new \DateTime($content['executionDate']));
            $transaction->setEndToEndId($content['endToEndId']);
            $transaction->setDigest($content['digest']);
            $transaction->setDescription($content['description']);
            $transaction->setCurrency($content['currency']);
            $transaction->setCreditorId($content['creditorId']);
            $transaction->setCounterpartReference($content['counterpartReference']);
            $transaction->setCounterpartName($content['counterpartName']);
            $transaction->setBankTransactionCode($content['bankTransactionCode']);
            $transaction->setAmount($content['amount']);
            $transaction->setAdditionalInformation($content['additionalInformation']);

            $this->documentManager->persist($transaction);
            $this->documentManager->flush();

            return new Response('');
        }

        return new Response('Invalid json', 500);
    }
}
