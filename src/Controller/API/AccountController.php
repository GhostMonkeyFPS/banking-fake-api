<?php

namespace EActive\Bundle\BankingFakeAPIBundle\Controller\API;

use Doctrine\ODM\MongoDB\DocumentManager;
use EActive\Bundle\BankingFakeAPIBundle\Collection\BookCollection;
use EActive\Bundle\BankingFakeAPIBundle\Document\Account;
use EActive\Bundle\BankingFakeAPIBundle\Security\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AccountController extends AbstractController
{
    /**
     * @var DocumentManager
     */
    private $documentManager;

    /**
     * @var UserService
     */
    private $userService;

    public function __construct(DocumentManager $documentManager, UserService $userService)
    {
        $this->documentManager = $documentManager;
        $this->userService = $userService;
    }

    public function listAccounts(Request $request): Response
    {
        if ('application/vnd.api+json' != $request->headers->get('Accept')) {
            return new Response('Invalid Accept header', 500);
        }
        if ('' == $request->headers->get('Authorization')) {
            return new Response('Empty Authorization header', 500);
        }
        $limit = $request->query->get('limit');
        if (null == $limit || ($limit < 0 || $limit > 100)) {
            $limit = 10;
        }

        $beforeAfter = 'after';
        $cursor = null;
        if ($result = $request->query->get('before')) {
            $cursor = $result;
            $beforeAfter = 'before';
        }
        if ($result = $request->query->get('after')) {
            $cursor = $result;
            $beforeAfter = 'after';
        }

        $user = $this->userService->getUserByToken($request->headers->get('Authorization'));

        $accounts = new BookCollection($user->getAccounts(), $cursor, $beforeAfter, $limit);

        $data = [];
        /** @var Account $account */
        foreach ($accounts as $account) {
            $data[] = [
                'type' => 'account',
                'relationships' => [
                    'transactions' => [
                        'links' => [
                            'related' => '',
                            'meta' => [
                                'type' => '',
                            ],
                        ],
                    ],
                    'financialInstitution' => [
                        'links' => [
                            'related' => '',
                            'data' => [
                                'type' => 'financialInstitution',
                                'id',
                            ],
                        ],
                    ],
                ],
                'meta' => [
                    'synchronizedAt' => $account->getSynchronizedAt()->format(\DateTime::ISO8601),
                    'latestSynchronization' => [
                        'type' => 'synchronization',
                        'id' => '0fb537de-456b-469d-aadd-68187dd8012b',
                        'attributes' => [
                            'updatedAt' => '2021-02-11T15:00:44.619Z',
                            'subtype' => 'accountDetails',
                            'status' => 'error',
                            'resourceType' => 'account',
                            'resourceId' => '30566675-268b-40f0-b278-7e41d899ff00',
                            'errors' => [
                                [
                                    'detail' => 'Authorization has expired. Please reauthorize the account.',
                                    'code' => 'authorizationExpired',
                                ],
                            ],
                            'createdAt' => '2021-02-11T15:00:44.451Z',
                        ],
                    ],
                    'availability' => 'available',
                ],
                'id' => $account->getId(),
                'attributes' => [
                    'subtype' => $account->getSubType(),
                    'referenceType' => $account->getReferenceType(),
                    'reference' => $account->getReference(),
                    'product' => $account->getProduct(),
                    'internalReference' => $account->getInternalReference(),
                    'holderName' => $account->getHolderName(),
                    'description' => $account->getDescription(),
                    'deprecated' => $account->isDeprecated(),
                    'currentBalanceVariationObservedAt' => $account->getCurrentBalanceVariationObservedAt()->format(\DateTime::ISO8601),
                    'currentBalanceReferenceDate' => $account->getCurrentBalanceReferenceDate()->format(\DateTime::ISO8601),
                    'currentBalanceChangedAt' => $account->getCurrentBalanceChangedAt()->format(\DateTime::ISO8601),
                    'currentBalance' => $account->getCurrentBalance(),
                    'currency' => $account->getCurrency(),
                    'availableBalanceVariationObservedAt' => $account->getAvailableBalanceVariationObservedAt()->format(\DateTime::ISO8601),
                    'availableBalanceReferenceDate' => $account->getCurrentBalanceReferenceDate()->format(\DateTime::ISO8601),
                    'availableBalanceChangedAt' => $account->getAvailableBalanceChangedAt()->format(\DateTime::ISO8601),
                    'availableBalance' => $account->getAvailableBalance(),
                    'authorizedAt' => (null != $account->getAuthorizedAt()) ? $account->getAuthorizedAt()->format(\DateTime::ISO8601) : null,
                    'authorizationExpirationExpectedAt' => ($account->getAuthorizationExpirationExpectedAt()) ? $account->getAuthorizationExpirationExpectedAt()->format(\DateTime::ISO8601) : null,
                ],
            ];
        }

        return new JsonResponse([
            'meta' => [
                'paging' => [
                    'limit' => 1,
                    'after' => '',
                ],
            ],
            'links' => [
                'next' => '',
                'first' => '',
            ],
            'data' => $data,
        ]);
    }
}
