<?php

namespace EActive\Bundle\BankingFakeAPIBundle\Controller\API;

use Doctrine\ODM\MongoDB\DocumentManager;
use EActive\Bundle\BankingFakeAPIBundle\Document\Synchronization;
use EActive\Bundle\BankingFakeAPIBundle\Model\SyncRequestModel;
use EActive\Bundle\BankingFakeAPIBundle\Security\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class SyncController extends AbstractController
{
    /**
     * @var UserService
     */
    private $userService;

    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * @var DocumentManager
     */
    private $documentManger;

    /**
     * SyncController constructor.
     */
    public function __construct(UserService $userService,
                                SerializerInterface $serializer,
                                ValidatorInterface $validator,
                                DocumentManager $documentManager
    ) {
        $this->userService = $userService;
        $this->serializer = $serializer;
        $this->validator = $validator;
        $this->documentManger = $documentManager;
    }

    public function createSyncRequest(Request $request): Response
    {
        $bearer = $this->authCheckCreate($request);

        /** @var SyncRequestModel $syncRequest */
        $syncRequest = $this->serializer->deserialize($request->getContent(), SyncRequestModel::class, 'json');

        $errors = $this->validator->validate($syncRequest);

        if (\count($errors) > 0) {
            return new JsonResponse(['error' => $errors[0]->getMessage()], 422);
        }

        $user = $this->userService->getUserByToken($bearer);

        $account = $this->userService->getAccountByUser($user, $syncRequest->getResourceId());

        $sync = new Synchronization();
        $sync->setSubType($syncRequest->getSubType());
        $sync->setResourceType($syncRequest->getResourceType());
        $sync->setResourceId($account->getId());
        $account->addSynchronization($sync);

        $this->documentManger->persist($account);
        $this->documentManger->flush();

        return new JsonResponse([
            'data' => [
                'type' => 'synchronization',
                'id' => $sync->getId(),
                'attributes' => [
                    'updatedAt' => $sync->getUpdatedAt()->format(\DateTime::ISO8601),
                    'subtype' => $sync->getSubType(),
                    'status' => $sync->getStatus(),
                    'resourceType' => $sync->getResourceType(),
                    'resourceId' => $sync->getResourceId(),
                    'errors' => $sync->getErrors()->toArray(),
                ],
            ],
        ]);
    }

    public function statusSync(Request $request, string $id): Response
    {
        $sync = $this->documentManger->getRepository(Synchronization::class)
            ->find($id);

        if (null == $sync) {
            return new JsonResponse(['error' => 'no sync object found'], 422);
        }

        $auth = $this->authCheckStatus($request);

        $user = $this->userService->getUserByToken($auth);

        if (!$user->getAccounts()->contains($sync->getAccount())) {
            return new JsonResponse(['error' => 'Unauthorized sync request'], 422);
        }

        return new JsonResponse([
            'data' => [
                'type' => 'synchronization',
                'id' => $sync->getId(),
                'attributes' => [
                    'updatedAt' => $sync->getUpdatedAt()->format(\DateTime::ISO8601),
                    'subtype' => $sync->getSubType(),
                    'status' => $sync->getStatus(),
                    'resourceType' => $sync->getResourceType(),
                    'resourceId' => $sync->getResourceId(),
                    'errors' => $sync->getErrors(),
                    'createdAt' => $sync->getCreatedAt()->format(\DateTime::ISO8601),
                ],
            ],
        ]);
    }

    private function authCheckStatus(Request $request): string
    {
        if ('application/vnd.api+json' != $request->headers->get('Accept')) {
            throw new HttpException(500, 'Invalid Accept header');
        }
        if (!$auth = $request->headers->get('Authorization')) {
            throw new HttpException(500, 'No Authorization header');
        }

        return $auth;
    }

    private function authCheckCreate(Request $request): string
    {
        if ('application/vnd.api+json' != $request->headers->get('content-type')) {
            throw new HttpException(500, 'Invalid Content-Type Header');
        }
        if ('application/vnd.api+json' != $request->headers->get('accept')) {
            throw new HttpException(500, 'Invalid Accept header');
        }
        if (!$auth = $request->headers->get('authorization')) {
            throw new HttpException(500, 'No Authorization header');
        }

        return $auth;
    }
}
