<?php

namespace EActive\Bundle\BankingFakeAPIBundle\Controller\API;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ODM\MongoDB\DocumentManager;
use EActive\Bundle\BankingFakeAPIBundle\Collection\BookCollection;
use EActive\Bundle\BankingFakeAPIBundle\Document\Transaction;
use EActive\Bundle\BankingFakeAPIBundle\Security\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class TransactionController extends AbstractController
{
    /**
     * @var DocumentManager
     */
    private $documentManager;

    /**
     * @var UserService
     */
    private $userService;

    public function __construct(DocumentManager $documentManager, UserService $userService)
    {
        $this->documentManager = $documentManager;
        $this->userService = $userService;
    }

    public function listTransactions(Request $request, string $accountId): Response
    {
        if ('application/vnd.api+json' != $request->headers->get('Accept')) {
            return new Response('Invalid Accept header', 500);
        }
        if ('' == $request->headers->get('Authorization')) {
            return new Response('Empty Authorization header', 500);
        }

        $limit = $request->query->get('limit');
        if (null == $limit || ($limit < 0 || $limit > 100)) {
            $limit = 10;
        }

        $beforeAfter = 'after';
        $cursor = null;
        if ($result = $request->query->get('before')) {
            $cursor = $result;
            $beforeAfter = 'before';
        }
        if ($result = $request->query->get('after')) {
            $cursor = $result;
            $beforeAfter = 'after';
        }

        $user = $this->userService->getUserByToken($request->headers->get('Authorization'));

        $account = $this->userService->getAccountByUser($user, $accountId);

        $transactions = $this->documentManager->getRepository(Transaction::class)
            ->getTransactionsByAccount($account);

        $transactionsCollection = new BookCollection(new ArrayCollection($transactions), $cursor, $beforeAfter, $limit);

        $data = [];
        /** @var Transaction $transaction */
        foreach ($transactionsCollection as $transaction) {
            $data[] = [
                'type' => 'transaction',
                'relationships' => [
                    'account' => [
                        'links' => [
                            'related' => '',
                        ],
                        'data' => [
                            'type' => 'account',
                            'id' => $account->getId(),
                        ],
                    ],
                ],
                'id' => $transaction->getId(),
                'attributes' => [
                    'valueDate' => $transaction->getValueDate()->format(\DateTime::ISO8601),
                    'remittanceInformationType' => $transaction->getRemittanceInformationType(),
                    'remittanceInformation' => $transaction->getRemittanceInformation(),
                    'purposeCode' => $transaction->getPurposeCode(),
                    'proprietaryBankTransactionCode' => $transaction->getProprietaryBankTransactionCode(),
                    'mandateId' => $transaction->getMandateId(),
                    'internalReference' => $transaction->getInternalReference(),
                    'executionDate' => $transaction->getExecutionDate()->format(\DateTime::ISO8601),
                    'endToEndId' => $transaction->getEndToEndId(),
                    'digest' => $transaction->getDigest(),
                    'description' => $transaction->getDescription(),
                    'currency' => $transaction->getCurrency(),
                    'creditorId' => $transaction->getCreditorId(),
                    'counterpartReference' => $transaction->getCounterpartReference(),
                    'counterpartName' => $transaction->getCounterpartName(),
                    'bankTransactionCode' => $transaction->getBankTransactionCode(),
                    'amount' => $transaction->getAmount(),
                    'additionalInformation' => $transaction->getAdditionalInformation(),
                ],
            ];
        }

        return new JsonResponse([
            'meta' => [
                'synchronizedAt' => '',
                'paging' => [
                    'limit' => $limit,
                    'before' => $transactionsCollection->getBeforeId(),
                    'after' => $transactionsCollection->getAfterId(),
                ],
                'latestSynchronization' => [
                    'type' => 'synchronization',
                    'id' => '',
                    'attributes' => [
                        'updatedAt' => '',
                        'subtype' => '',
                        'status' => '',
                        'resourceType' => '',
                        'resourceId' => '',
                        'errors' => [
                            [
                                'detail' => '',
                                'code' => '',
                            ],
                        ],
                        'createdAt' => '',
                    ],
                ],
            ],
            'links' => [
                'next' => '',
                'first' => '',
            ],
            'data' => $data,
        ]);
    }
}
