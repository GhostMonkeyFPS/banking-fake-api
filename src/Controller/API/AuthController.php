<?php

namespace EActive\Bundle\BankingFakeAPIBundle\Controller\API;

use Doctrine\ODM\MongoDB\DocumentManager;
use EActive\Bundle\BankingFakeAPIBundle\Document\Auth\Token;
use EActive\Bundle\BankingFakeAPIBundle\Document\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AuthController extends AbstractController
{
    /**
     * @var DocumentManager
     */
    private $documentManager;

    /**
     * AuthController constructor.
     */
    public function __construct(DocumentManager $documentManager)
    {
        $this->documentManager = $documentManager;
    }

    public function getToken(Request $request)
    {
        if ('application/x-www-form-urlencoded' != $request->headers->get('Content-Type')) {
            return new Response('Invalid Content-Type Header', 500);
        }
        if ('application/vnd.api+json' != $request->headers->get('Accept')) {
            return new Response('Invalid Accept header', 500);
        }
        if (!$auth = $request->headers->get('Authorization')) {
            return new Response('No Authorization header', 500);
        }

        if (!$grantType = $request->request->get('grant_type')) {
            return new Response('No grant_type', 500);
        }

        if ('authorization_code' != $grantType && 'refresh_token' != $grantType) {
            return new Response('Invalid grant_type: '.$grantType, 500);
        }

        $repo = $this->documentManager->getRepository(User::class);
        if ('authorization_code' == $grantType) {
            if (!$code = $request->request->get('code')) {
                return new Response('No code provided', 500);
            }

            /** @var User $user */
            $user = $repo->findUserByInitialToken($code);

            $tokens = $user->getTokens();

            return new JsonResponse([
                'access_token' => $tokens->getAccessToken(),
                'expires_in' => 1799,
                'refresh_token' => $tokens->getRefreshToken(),
                'scope' => 'offline_access ai pi name',
                'token_type' => 'bearer',
            ]);
        } else {
            /** @var User $user */
            $user = $repo->findUserByRefreshToken($request->request->get('refresh_token'));

            $tokens = new Token();
            $tokens->setInitialToken($this->generateRandomString(87));
            $tokens->setAccessToken($this->generateRandomString(87));
            $tokens->setRefreshToken($this->generateRandomString(87));

            $user->setTokens($tokens);

            $this->documentManager->persist($user);
            $this->documentManager->flush();

            return new JsonResponse([
                'access_token' => $tokens->getAccessToken(),
                'expires_in' => 1799,
                'refresh_token' => $tokens->getRefreshToken(),
                'scope' => 'offline_access ai pi name',
                'token_type' => 'bearer',
            ]);
        }
    }

    protected function generateRandomString(int $length): string
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_.-';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; ++$i) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        return $randomString;
    }
}
