<?php

namespace EActive\Bundle\BankingFakeAPIBundle\Controller;

use Doctrine\ODM\MongoDB\DocumentManager;
use EActive\Bundle\BankingFakeAPIBundle\Document\Account;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Exception\NotNormalizableValueException;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class AccountController extends AbstractController
{
    /**
     * @var DocumentManager
     */
    private $documentManager;

    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    public function __construct(DocumentManager $documentManager,
                                SerializerInterface $serializer,
                                ValidatorInterface $validator
    ) {
        $this->documentManager = $documentManager;
        $this->serializer = $serializer;
        $this->validator = $validator;
    }

    public function index(): Response
    {
        return $this->render('@BankingFakeAPI/account/index.html.twig');
    }

    public function createAccount(Request $request): JsonResponse
    {
        try {
            $account = $this->serializer->deserialize($request->getContent(), Account::class, 'json');
        } catch (NotNormalizableValueException $exception) {
            return new JsonResponse(['error' => $exception->getMessage()], 422);
        }

        $errors = $this->validator->validate($account);

        if (\count($errors) > 0) {
            return new JsonResponse(['error' => $errors[0]->getMessage()], 422);
        }

        $this->documentManager->persist($account);
        $this->documentManager->flush();

        return new JsonResponse(['status' => 'ok'], 200);
    }

    public function updateAccount(string $id): Response
    {
        $account = $this->documentManager->getRepository(Account::class)
            ->find($id);

        return $this
            ->render('@BankingFakeAPI/account/info.html.twig',
                [
                    'account' => $account,
                ]);
    }

    public function updateAccountPost(Request $request, string $id): JsonResponse
    {
        /** @var Account $account */
        $account = $this->documentManager->getRepository(Account::class)
            ->find($id);

        if (null == $account) {
            throw $this->createNotFoundException('No account found for id '.$id);
        }

        try {
            /** @var Account $serializedAccount */
            $serializedAccount = $this->serializer->deserialize($request->getContent(), Account::class, 'json');
        } catch (NotNormalizableValueException $exception) {
            return new JsonResponse(['error' => $exception->getMessage()], 422);
        }

        $errors = $this->validator->validate($serializedAccount);

        if (\count($errors) > 0) {
            return new JsonResponse(['error' => $errors[0]->getMessage()], 422);
        }

        $account->setSubType($serializedAccount->getSubType());
        $account->setReferenceType($serializedAccount->getReferenceType());
        $account->setReference($serializedAccount->getReference());
        $account->setProduct($serializedAccount->getProduct());
        $account->setHolderName($serializedAccount->getHolderName());
        $account->setDescription($serializedAccount->getDescription());
        $account->setCurrentBalanceReferenceDate($serializedAccount->getCurrentBalanceReferenceDate());
        $account->setCurrentBalanceChangedAt($serializedAccount->getCurrentBalanceChangedAt());
        $account->setCurrentBalance($serializedAccount->getCurrentBalance());
        $account->setCurrency($serializedAccount->getCurrency());
        $account->setAvailableBalanceReferenceDate($serializedAccount->getAvailableBalanceReferenceDate());
        $account->setAvailableBalanceChangedAt($serializedAccount->getAvailableBalanceReferenceDate());
        $account->setAvailableBalance($serializedAccount->getAvailableBalance());

        $this->documentManager->flush();

        return new JsonResponse(['status' => 'ok'], 200);
    }
}
