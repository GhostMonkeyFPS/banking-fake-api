<?php

namespace EActive\Bundle\BankingFakeAPIBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ODM\MongoDB\DocumentManager;
use EActive\Bundle\BankingFakeAPIBundle\Document\Account;
use EActive\Bundle\BankingFakeAPIBundle\Document\Auth\Token;
use EActive\Bundle\BankingFakeAPIBundle\Document\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AuthController extends AbstractController
{
    /**
     * @var DocumentManager
     */
    private $documentManager;

    /**
     * AuthController constructor.
     */
    public function __construct(DocumentManager $documentManager)
    {
        $this->documentManager = $documentManager;
    }

    public function index(): Response
    {
        return $this
            ->render('@BankingFakeAPI/auth/index.html.twig');
    }

    public function createAccount(): Response
    {
        $repo = $this->documentManager->getRepository(Account::class);

        $accounts = $repo->findAll();

        return $this
            ->render('@BankingFakeAPI/auth/create_account.html.twig', ['accounts' => $accounts]);
    }

    public function createAccountPost(Request $request): Response
    {
        if (!$url = $request->query->get('redirect_uri')) {
            return new Response('No redirect_uri provided', 500);
        }
        if (!$state = $request->query->get('state')) {
            return new Response('No state provided', 500);
        }

        $repo = $this->documentManager->getRepository(Account::class);

        $content = $request->request->all();

        $accounts = [];
        foreach ($content as $key => $value) {
            if ($account = $repo->find($key)) {
                $accounts[] = $account;
            } else {
                throw new \Exception('Account not found');
            }
        }

        $user = new User();
        $user->setAccounts(new ArrayCollection($accounts));

        $token = new Token();
        $token->setInitialToken($this->generateRandomString(87));
        $token->setAccessToken($this->generateRandomString(87));
        $token->setRefreshToken($this->generateRandomString(87));

        $user->setTokens($token);

        $this->documentManager->persist($user);
        $this->documentManager->flush();

        $url .= '?state='.$state;
        $url .= '&code='.$token->getInitialToken();

        return new RedirectResponse($url);
    }

    protected function generateRandomString(int $length): string
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_.-';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; ++$i) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        return $randomString;
    }
}
