<?php

namespace EActive\Bundle\BankingFakeAPIBundle\Security;

use Doctrine\ODM\MongoDB\DocumentManager;
use EActive\Bundle\BankingFakeAPIBundle\Document\Account;
use EActive\Bundle\BankingFakeAPIBundle\Document\User;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

class UserService
{
    /**
     * @var DocumentManager
     */
    private $documentManager;

    public function __construct(DocumentManager $documentManager)
    {
        $this->documentManager = $documentManager;
    }

    /**
     * @throws UnauthorizedHttpException
     */
    public function getUserByToken(string $token): User
    {
        $token = str_replace('Bearer ', '', $token);

        $repo = $this->documentManager->getRepository(User::class);

        /** @var User $user */
        $user = $repo->findUserByBearer($token);

        if (null == $user) {
            throw new UnauthorizedHttpException('Invalid bearer');
        }

        if (time() > $user->getTokens()->getRefreshTokenCreatedAt()->getTimestamp() + strtotime('+15 minutes')) {
            throw new UnauthorizedHttpException('Expired bearer');
        }

        return $user;
    }

    /**
     * @throws UnauthorizedHttpException
     */
    public function getAccountByUser(User $user, string $accountId): Account
    {
        $account = $this->documentManager->getRepository(Account::class)
            ->find($accountId);

        if (!$user->getAccounts()->contains($account)) {
            throw new UnauthorizedHttpException('Account not found');
        }


        return $account;
    }
}
